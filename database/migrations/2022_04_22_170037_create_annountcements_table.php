<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnountcementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('annountcements', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->text('remark')->nullable();
            $table->string('status')->nullable();
            $table->string('image')->nullable();
            $table->datetime('start_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('annountcements');
    }
}
