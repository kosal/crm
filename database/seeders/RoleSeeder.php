<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Exception;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles =  [
			[
				'name' => 'POSITION',
				'permission' => [
					['name' => 'create_position'],
					['name' => 'update_position'],
					['name' => 'delete_position'],
					['name' => 'view_position'],
				]
			],
			[
				'name' => 'USER',
				'permission' => [
					['name' => 'create_user'],
					['name' => 'update_user'],
					['name' => 'delete_user'],
					['name' => 'view_user'],
				]
			],
			[
				'name' => 'ROLE',
				'permission' => [
					['name' => 'create_role'],
					['name' => 'update_role'],
					['name' => 'delete_role'],
					['name' => 'view_role'],
				]
			],
			[
				'name' => 'LOCATION',
				'permission' => [
					['name' => 'create_location'],
					['name' => 'update_location'],
					['name' => 'delete_location'],
					['name' => 'view_location'],
				]
			],
			[
				'name' => 'BRANCH',
				'permission' => [
					['name' => 'create_branch'],
					['name' => 'update_branch'],
					['name' => 'delete_branch'],
					['name' => 'view_branch'],
				]
			],
			[
				'name' => 'CUSTOMER',
				'permission' => [
					['name' => 'create_customer'],
					['name' => 'update_customer'],
					['name' => 'delete_customer'],
					['name' => 'view_customer'],
				]
			],
			[
				'name' => 'APPOINTMENT',
				'permission' => [
					['name' => 'create_appointment'],
					['name' => 'update_appointment'],
					['name' => 'delete_appointment'],
					['name' => 'view_appointment'],
				]
			],
			[
				'name' => 'PRODUCT',
				'permission' => [
					['name' => 'create_product'],
					['name' => 'update_product'],
					['name' => 'delete_product'],
					['name' => 'view_product'],
				]
			],
		];	

		foreach ($roles as $role) {
			$pObj = [];
			foreach ($role['permission'] as $permission) {
				$ePermission = Permission::where('name', $permission['name'])->first();
				if(!$ePermission){
					$ePermission = Permission::create($permission);
				}
				$pObj[] = $ePermission;
			}

			if(!empty($pObj)){
				$roleName = $role['name'];
				try{
		            $role = Role::findByName($roleName);
		            $role->update(['title' => Str::headline(Str::lower($roleName))]);
		        }catch(Exception $exception){
		            $role =  Role::create(['name' => $roleName, 'title' => Str::headline(Str::lower($roleName))]);
		        }
		        $role->syncPermissions($pObj);
			}
		}
    }
}
