<?php

namespace Database\Seeders;

use App\Models\CustomerType;
use Illuminate\Database\Seeder;

class CustomerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CustomerType::create([
            'name'=> 'IT',
            'remark'=> 'test',
            'status'=> 'enabled'
        ]);
    }
}
