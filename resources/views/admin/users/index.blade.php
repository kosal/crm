@extends('layouts.app')
@section('content_header')
    <div class="row">
        <div class="col-md-12">
            <h4 class="float-left">{{get_page_title()}}</h4>
            <a href="{{ get_create_link() }}" class="btn btn-primary btn-sm float-right">{!!get_create_title()!!}</a>
        </div>
    </div>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {!!message_render()!!}
             <div class="card">
               <div class="card-header">
                  <h3 class="card-title">{{get_page_title()}}</h3>
               </div>
               <div class="card-body">
                    {{Form::open(['url'=>get_link_list(), 'method'=>'GET'])}}
                        <table class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                   @foreach(get_table_column() as $col)
                                      <th class="text-uppercase">
                                         {{$col}}
                                      </th>
                                   @endforeach
                                </tr>
                            </thead>
                            <tbody>
                                @if($rows->count())
                                   @foreach($rows as $row)
                                      <tr>
                                         <td>
                                            {!! $row->id !!}
                                         </td>
                                         <td>
                                            <a href="{{$row->edit_link}}">
                                                {!! $row->name !!}
                                            </a>
                                         </td>
                                         <td>
                                            {!! $row->email !!}
                                         </td>
                                         <td class="text-right">
                                            {!! $row->btn_show !!}
                                            {!! $row->btn_edit !!}
                                            @if($row->id != super_admin_id() && $row->id != auth()->id())
                                               {!! $row->btn_delete !!}
                                            @endif
                                         </td>
                                      </tr>
                                   @endforeach
                                @else
                                   <tr><td colspan="4">{{__('There is no data available.')}}</td></tr>
                                @endif
                            </tbody>
                        </table>
                    {{Form::close()}}
               </div>
               <div class="card-footer clearfix">
                    {!! $rows->render() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script> console.log('Hi!'); </script>
@stop
