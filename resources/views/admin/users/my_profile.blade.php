@extends('layouts.app')
@section('content_header')
    <div class="row">
        <div class="col-md-12">
            <h4 class="float-left">{{get_page_title()}}</h4>
        </div>
    </div>
@stop
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            {!!message_render()!!}
             <div class="card">
               <div class="card-header">
                  <h3 class="card-title">{{get_page_title()}}</h3>
               </div>
               <div class="card-body">
                    {{Form::open(['url'=>get_form_url(), 'method'=>get_form_method()])}}
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">{{__('Name')}}</label>
                            <div class="col-sm-10">
                                {{Form::text('name', auth()->user()->name??'', ['class'=>'form-control', 'id'=>'name', 'placeholder'=>__('Name'), 'required' => true])}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
                            <div class="col-sm-10">
                                {{Form::email('email', auth()->user()->email??'', ['class'=>'form-control', 'id'=>'email', 'placeholder'=>__('Email'), 'required' => true])}}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button class="btn btn-primary float-right">{{__('Save')}}</button>
                            <a href="{{get_link_list()}}" class="btn btn-default float-right mr-1">{{__('Close')}}</a>
                        </div>
                    {{Form::close()}}
               </div>
            </div>
        </div>
    </div>
</div>
@endsection
