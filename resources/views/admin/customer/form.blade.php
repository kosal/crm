@extends('layouts.app')

@section('content')
<div class="container py-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
             <div class="card">
               <div class="card-header">
                  <h3 class="card-title">{{get_page_title()}}</h3>
                  <div class="card-tools">
                     <a href="{{ get_create_link() }}" class="btn btn-tool btn-sm text-primary">{!!get_create_title()!!}</a>
                  </div>
               </div>
               <div class="card-body">
                    {{Form::open(['url' => !empty($row->id)?route('admin.customer_types.update', $row->id):route('admin.customer_types.store'), 'method'=>!empty($row->id)?'PUT':'POST'])}}
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">{{__('Name')}}</label>
                            <div class="col-sm-10">
                                {{Form::text('name', $row->name??'', ['class'=>'form-control', 'id'=>'name', 'placeholder'=>__('Name'), 'required' => true])}}
                            </div>
                        </div>
                        <div class="form-group row">
                           <label for="remark" class="col-sm-2 col-form-label">{{__('Remark')}}</label>
                           <div class="col-sm-10">
                               {{Form::text('remark', $row->remark??'', ['class'=>'form-control', 'id'=>'remark', 'placeholder'=>__('Remark')])}}
                           </div>
                        </div>
                        <div class="form-group row">
                           <label for="status" class="col-sm-2 col-form-label">{{__('Status')}}</label>
                           <div class="col-sm-10">
                               {{Form::select('status', ['enabled'=>'Enabled', 'disabled'=> 'Disabled'], $row->status??'', ['class'=>'form-control', 'id'=>'status'])}}
                           </div>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-primary float-right">{{__('Save')}}</button>
                            <a href="{{route('admin.customer_types.index')}}" class="btn btn-default float-right mr-1">{{__('Close')}}</a>
                        </div>
                    {{Form::close()}}
               </div>
            </div>
        </div>
    </div>
</div>
@endsection
