@extends('layouts.app')

@section('content')
<div class="container py-3">
    <div class="row justify-content-center">
        <div class="col-md-12">
             <div class="card">
               <div class="card-header">
                  <h3 class="card-title">{{get_page_title()}}</h3>
                  <div class="card-tools">
                     <a href="{{ get_create_link() }}" class="btn btn-tool btn-sm text-primary">{!!get_create_title()!!}</a>
                  </div>
               </div>
               <div class="card-body p-0">
                  <table class="table table-striped projects">
                     <thead>
                        <tr>
                           @foreach(get_table_column() as $col)
                              <th class="text-uppercase">
                                 {{$col}}
                              </th>
                           @endforeach
                        </tr>
                     </thead>
                     <tbody>
                        @if ($rows->count())
                           @foreach ($rows as $row)
                           <tr class="item-{{$row->id}}">
                              <td>
                                 {{$row->id}}
                              </td>
                              <td>
                                 <a>
                                 {{$row->name}}
                                 </a>
                              </td>
                              <td>
                                 {{$row->remark}}
                              </td>
                              <td >
                                 <span class="badge badge-success text-uppercase">{{$row->status}}</span>
                              </td>
                              <td class="project-actions text-right">
                                 <a class="btn btn-info btn-sm" href="{{route('admin.customer_types.edit', $row->id)}}">
                                 <i class="fas fa-pencil-alt"></i>
                                 Edit
                                 </a>
                                 <a class="btn btn-danger btn-sm" href="#" data-id="{{ $row->id }}" onclick="deleteItem(this)" data-url="{{route('admin.customer_types.destroy', $row->id)}}">
                                 <i class="fas fa-trash"></i>
                                 Delete
                                 </a>
                              </td>
                           </tr> 
                           @endforeach
                        @else
                           <tr>
                              <td colspan="5" class="text-center">{{__('No data')}}</td>
                           </tr>
                        @endif
                     </tbody>
                  </table>
               </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('js')
    <script>
       $(document).ready(function(){
         // test();

       });
       function test(){
          alert(123);
       }
       function deleteItem(obj){
         var id = $(obj).data('id');
         var url = $(obj).data('url');
         var token = $('meta[name=csrf-token]').attr('content');
         $('.item-'+ id).remove();
         $.ajax({
            type:'delete', 
            url: url,
            data: {
               _token:token
            },
            dataType: 'json',
            success: function(result){
               console.log(result)
            },
            error: function(result){
               console.log(result)
            }
         }); 
       }
    </script>
@stop
