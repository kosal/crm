<?php 

use Illuminate\Support\Facades\Route;

Route::get('admin', [App\Http\Controllers\HomeController::class, 'index'])->name('admin');

Route::group(['middleware' => 'auth', 'prefix' => 'admin', 'as' => 'admin.'], function () {

	Route::get('my_profile', 'MyProfileController@getMyProfile')->name('get_my_profile');
	Route::post('my_profile', 'MyProfileController@postMyProfile')->name('post_my_profile');

	Route::get('change_password', 'MyProfileController@getChangePassword')->name('get_change_password');
	Route::post('change_password', 'MyProfileController@postChangePassword')->name('post_change_password');
	Route::get('logout', 'MyProfileController@logout')->name('logout');

	Route::resource('customer_types', CustomerTypeController::class);
	Route::resource('users', UserController::class);
	Route::resource('customers', CustomerController::class);
});
