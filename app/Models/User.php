<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    const SUPER_ADMIN_ID = 1;
    
    use HasApiTokens, HasFactory, Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getBtnDeleteAttribute(){
        return '<a class="btn btn-danger btn-sm" href="'.route('admin.users.destroy', $this->id).'">
                    <i class="fas fa-trash"></i>
                '.__('Delete').'
                </a>';
    }

    public function getBtnEditAttribute(){
        return '<a class="btn btn-info btn-sm" href="'.$this->edit_link.'">
                    <i class="fas fa-pencil-alt"></i>
                '.__('Edit').'
                </a>';
    }

    public function getBtnShowAttribute(){
        return '<a class="btn btn-primary btn-sm" href="'.route('admin.users.show', $this->id).'">
                    <i class="fas fa-eye"></i>
                '.__('Show').'
                </a>';
    }

    public function getEditLinkAttribute(){
        return route('admin.users.edit', $this->id);
    }
}
