<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Annountcement extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'remark',
        'image',
        'status',
        'start_date',
    ];
}
