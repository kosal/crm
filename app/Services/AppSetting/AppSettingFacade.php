<?php

namespace App\Services\AppSetting;

use Illuminate\Support\Facades\Facade;

class AppSettingFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'AppSetting';
    }
}
