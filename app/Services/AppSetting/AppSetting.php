<?php

namespace App\Services\AppSetting;


class AppSetting
{
    protected $title = '';
    protected $a_title = '';
    protected $a_link = '';
    protected $table_colunm = [];
    protected $form_url = '';
    protected $form_method = '';
    protected $link_list = '';

    function __construct(){
        
    }

    public function setPageTitle($str)
    {
        $this->title = $str;
    }

    public function getPageTitle()
    {
        return $this->title;
    }

    public function setANewTitle($str)
    {
        $this->a_title = $str;
    }

    public function getANewTitle()
    {
        return $this->a_title;
    }

    public function setALink($str)
    {
        $this->a_link = $str;
    }

    public function getALink()
    {
        return $this->a_link;
    }

    public function setTableColumn($arr)
    {
        $this->table_colunm = $arr;
    }

    public function getTableColumn()
    {
        return $this->table_colunm;
    }

    public function setFormUrl($str)
    {
        $this->form_url = $str;
    }

    public function getFormUrl()
    {
        return $this->form_url;
    }

    public function setFormMethod($str)
    {
        $this->form_method = $str;
    }

    public function getFormMethod()
    {
        return $this->form_method;
    }

    public function setLinkList($str)
    {
        $this->link_list = $str;
    }

    public function getLinkList()
    {
        return $this->link_list;
    }

}
