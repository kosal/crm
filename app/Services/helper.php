<?php

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use AppSetting as AppSettings;
use App\Models\User;

if (! function_exists('setting_date_format')) {
    function setting_date_format($input, $format='d/m/Y') {
        date_default_timezone_set(config('app.timezone'));
        $carbon = new \Carbon\Carbon;
        return $carbon->parse($input)->format($format);
    }
}

if (! function_exists('img_url')) {
    function img_url($filename, $size = 'original') {
    	$filename= Str::of($filename)->explode('/')->last();
    	if(!$filename){
    		return url('logo.png');
    	}
        return url('media/'.$size.'/'.$filename);
    }
}

if (! function_exists('set_page_title')) {
    function set_page_title($title = '') {
        if($title != ''){
            AppSettings::setPageTitle($title);
        }
    }
}

if (! function_exists('get_page_title')) {
    function get_page_title() {
        return AppSettings::getPageTitle();
    }
}

if (! function_exists('set_create_title')) {
    function set_create_title($title) {
        AppSettings::setANewTitle($title);
    }
}

if (! function_exists('get_create_title')) {
    function get_create_title() {
        return AppSettings::getANewTitle();
    }
}

if (! function_exists('set_create_link')) {
    function set_create_link($link='#') {
        AppSettings::setALink($link);
    }
}

if (! function_exists('get_create_link')) {
    function get_create_link() {
        return AppSettings::getALink();
    }
}

if (! function_exists('set_table_column')) {
    function set_table_column($columns) {
        AppSettings::setTableColumn($columns);
    }
}

if (! function_exists('get_table_column')) {
    function get_table_column() {
        return AppSettings::getTableColumn();
    }
}

if (! function_exists('super_admin_id')) {
    function super_admin_id() {
        return User::SUPER_ADMIN_ID;
    }
}

if (! function_exists('message_render')) {
    function message_render() {
        return view('admin.common.message');
    }
}

if (! function_exists('set_form_url')) {
    function set_form_url($str) {
        AppSettings::setFormUrl($str);
    }
}

if (! function_exists('get_form_url')) {
    function get_form_url() {
        return AppSettings::getFormUrl();
    }
}

if (! function_exists('set_form_method')) {
    function set_form_method($str) {
        AppSettings::setFormMethod($str);
    }
}

if (! function_exists('get_form_method')) {
    function get_form_method() {
        return AppSettings::getFormMethod();
    }
}

if (! function_exists('set_link_list')) {
    function set_link_list($str) {
        AppSettings::setLinkList($str);
    }
}

if (! function_exists('get_link_list')) {
    function get_link_list() {
        return AppSettings::getLinkList();
    }
}

