<?php

namespace App\Http\Controllers;

use App\Models\CustomerType;
use Illuminate\Http\Request;

class CustomerTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        set_page_title(__('Customer Type'));
        set_create_title(__('Create New'));
        set_create_link(route('admin.customer_types.create'));
        set_table_column(['id', 'name', 'remark', 'status', '']);
        $rows = CustomerType::paginate(10);

        return view('admin.customer_type.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_page_title(__('Customer Type'));
        set_create_title(__('Create New'));
        set_create_link(route('admin.customer_types.create'));

        return view('admin.customer_type.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'name'=> $request->name,
            'remark'=> $request->remark,
            'status'=> $request->status
        ];
        CustomerType::create($data);
        return redirect(route('admin.customer_types.index'))->with([
            'message'=> __('Save Successfully!')
        ]);
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        set_page_title(__('Customer Type'));
        set_create_title(__('Create New'));
        set_create_link(route('admin.customer_types.create'));
        $row= CustomerType::find($id);

        return view('admin.customer_type.form', compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row= CustomerType::find($id);
        $row->name= $request->name;
        $row->remark= $request->remark;
        $row->status= $request->status;
        $row->save();
        return redirect(route('admin.customer_types.index'))->with([
            'message'=> __('Update Successfully!')
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $row=CustomerType::find($id);
        if(!$row){
            return response()->json([
                'message'=>'Not Found!'
            ], 422);
        }
        $row->delete();
        return response()->json([
            'message'=> 'Delete Successfully!'
        ], 200);
    }
}
