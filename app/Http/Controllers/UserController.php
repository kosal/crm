<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
    	$limit = $request->get('limit', 10);

        set_page_title(__('User'));
        set_create_title(__('Create New'));
        set_create_link(route('admin.users.create'));
        set_table_column(['id', 'name', 'email', '']);
        set_link_list(route('admin.users.index'));

        $rows = User::paginate($limit)->withQueryString();
        return view('admin.users.index', compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        set_page_title(__('Create User'));
        set_create_title(__('Create New'));
        set_create_link(route('admin.users.create'));
        set_form_url(route('admin.users.store'));
        set_form_method('POST');
        set_link_list(route('admin.users.index'));

        return view('admin.users.form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = validator()->make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password'=> 'required|min:6|max:32',
            'password_confirmation' => 'required|same:password|min:6|max:32',
        ]);
        if ($validator->fails()){
            return back()->with(['error' => $validator->errors()->first()])->withInput();
        }

        $row = new User();
        $row->name = $request->name;
        $row->email = $request->email;
        $row->password = bcrypt($request->password);
        $row->save();

        // Adding roles to a user
        $roles = $request->get('roles', []);
        $this->userSyncRoles($roles, $row);

        return redirect(route('admin.users.index'))->with(['success' => __('Saved Successfully!')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = User::findOrFail((int)$id);

        set_page_title(__('Edit User'));
        set_create_title(__('Create New'));
        set_create_link(route('admin.users.create'));
        set_form_url(route('admin.users.update', $id));
        set_form_method('PUT');
        set_link_list(route('admin.users.index'));

        return view('admin.users.form', compact('row'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $row = User::findOrFail((int)$id);

        $validator = validator()->make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            // 'password'=> 'required|min:6|max:32',
            // 'password_confirmation' => 'required|same:password|min:6|max:32',
        ]);
        if ($validator->fails()){
            return back()->with(['error' => $validator->errors()->first()])->withInput();
        }

        if($id == super_admin_id()){
            return back()->with(['error' => __('Not allow to modify super admin information.')])->withInput();
        }

        $row->name = $request->name;
        $row->email = $request->email;
        $row->save();

        // Adding roles to a user
        $roles = $request->get('roles', []);
        $this->userSyncRoles($roles, $row);
        
        return redirect(route('admin.users.index'))->with(['success' => __('Updated Successfully!')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($id == super_admin_id()){
            return back()->with(['error' => __('Not allow to modify super admin information.')]);
        }

        $row = User::findOrFail((int)$id);

        $row->delete();
        return redirect(route('admin.users.index'))->with(['success' => __('Deleted Successfully!')]);
    }

    private function userSyncRoles($roles, $user){

        if(!empty($roles)){
            $roleIds = [];
            foreach ($roles as $role) {
                $roleIds[] = $role['id']??0;
            }
            $roleObjs = Role::find($roleIds);
            $user->syncRoles($roleObjs);
        }
    }
}
